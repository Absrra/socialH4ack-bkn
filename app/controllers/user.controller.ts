import { Express, Request, Response, Router } from 'express';
import { IExercise, Exercise } from '../models/exercise.model';

var router = Router();

export class UserController {

    constructor(){

    }

    public create(req: Request, res: Response) {
        
        Exercise.create(req.body).then((response) => {
            res.status(200).send({
                message: 'Datos almacenados',
                content: response
            })
        }).catch((error) => {
            res.status(500).send({
                content: error
            })
        })
        
    }

    /**
     * find
     */
    public find(req: Request, res: Response) {
        Exercise.find().then((response) => {
            res.status(200).send({
                message: 'Datos obtenidos',
                content: response
            })
        }).catch((error)    =>  {
            res.status(500).send({
                content: error
            })
        })
    }

    /**
     * findOne
     */
    public findById(req: Request, res: Response) {
        if (req.params.id === undefined || req.params.id === null) {
            res.status(400).send({
                message: 'Id no suminstrado'
            })
        } else {

            Exercise.findById(req.params.id).then((response) => {
                if (response === null) {
                    res.status(404).send({
                        message: 'id no encontrado'
                    })
                } else {
                    res.status(200).send({
                        message: 'Datos extraidos',
                        content: response
                    })
                }
            }).catch((error) => {
                res.status(500).send({
                    content: error
                })
            })

        }
    }

    /**
     * update
     */
    public update(req: Request, res: Response) {

        if (req.params.id === undefined || req.params.id === null) {
            res.status(400).send({
                message: 'Id no suminstrado'
            })
        } else {

            Exercise.update({_id: req.params.id}, req.body).then((response) => {
                res.status(200).send({
                    message: 'Datos actualizados',
                    content: response
                })
            }).catch((error) => {
                res.status(500).send({
                    content: error
                })
            });

        }

    }

    /**
     * destroy
     */
    public destroy(req: Request, res: Response) {


        Exercise.findByIdAndRemove(req.params.id).then((response) => {
            if (response === null) {
                res.status(404).send({
                    message: 'Elemento no encontrado',
                })
            } else {

                res.status(200).send({
                    message: 'Dato eliminado',
                    content: response
                })
                
            }
        }).catch((error) => {
            res.status(500).send({
                content: error
            })
        })

    }



    
}