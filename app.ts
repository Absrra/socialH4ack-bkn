import * as express from 'express';
import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as logger from "morgan";
import * as path from "path";
import * as cors from "cors";
import errorHandler = require("errorhandler");
import methodOverride = require("method-override");

/**
 * imports de los controlladores
 */
import { IndexRouter } from './app/router/index.router';



export class App {

    
    public app: express.Application;

    public static bootstrap(): App {
        return new App();
    }

    constructor(){
        this.app = express();

        //configure application
        this.config();

        //add routes
        this.routes();

        //add api
        this.api();
    }

    public config(): any {
              //add static paths
        this.app.use(express.static(path.join(__dirname, "public")));

        // configure pug
        // this.app.set("views", path.join(__dirname, "views"));
        // this.app.set("view engine", "pug");

        //use logger middlware
        this.app.use(logger("dev"));

        //use json form parser middlware
        this.app.use(bodyParser.json());

        //use query string parser middlware
        this.app.use(bodyParser.urlencoded({
          extended: true
        }));
    
        //use cookie parser middleware
        this.app.use(cookieParser("SECRET_GOES_HERE"));
    
        //use override middlware
        this.app.use(methodOverride());
    
        //catch 404 and forward to error handler
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });
    
        //error handling
        this.app.use(errorHandler());
    }

    private routes(): any {
        let router: express.Router;
        /**
         * configuración de los cors
         */
        const options:cors.CorsOptions = {
            allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
            credentials: true,
            methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
            origin: '*',
            preflightContinue: false
          };
        router = express.Router();
        router.use(cors(options));
        router.options("*", cors(options));

        IndexRouter.create(router);
        this.app.use(router);
    }
    public api(): any {
        
    }

    

}
